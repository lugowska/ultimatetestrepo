package football;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class PlayerRepository extends AbstractRepository<Player> {

	private static final String FILE_NAME = "players.csv";

	DateFormat df = new SimpleDateFormat("dd.MM.yyyy");
	@Override
	protected String getFileName() {
		return FILE_NAME;
	}
	public PlayerRepository() {
		super();
	}
	
	@Override
	public void add(Player item) {
		super.add(item);
	}
	
	@Override
	protected String getFileLineFromEntity(Player player) {
		List<String> words = new ArrayList<>();
		words.add(player.getId().toString());
		words.add(player.getFirstName());
		words.add(player.getLastName());
		words.add(df.format(player.getDateOfBirth()));
		words.add(String.valueOf(player.getSpeed()));
		words.add(player.getTeamId().toString());
		String fileLine = String.join(",", words);
		return fileLine;
	}
	@Override
	protected Player createEntity(String fileLine) {
		List<String> words = MyUtils.splitToTrimmedList(fileLine, ",");
		Integer id = MyUtils.getIntegerFromString(words.get(0), "Ostrze�enie 1");
		String firstName = words.get(1);
		String lastName = words.get(2);
		Date dateOfBirth = MyUtils.getDateFromText(words.get(3), "Ostrze�enie 2");
		int speed = MyUtils.getIntFromString(words.get(4), "Ostrze�enie 3");
		Integer teamId = MyUtils.getIntegerFromString(words.get(5), "Ostrze�enie 4");
		
		return new Player(id, firstName, lastName, dateOfBirth, speed, teamId);
	}
	
	@Override
	protected Player copy(Player player) {
		return new Player(player);
	}
	
	@Override
	public void saveEntitiesToFile() {
		
	}
	@Override
	public Player get(int id) {
		// TODO Auto-generated method stub
		return super.get(id);
	}
	@Override
	public void delete(int id) {
		// TODO Auto-generated method stub
		super.delete(id);
	}
	
	public void updateFieldsValues(Player playerToBeUpdated, Player player) {
		playerToBeUpdated.setDateOfBirth(player.getDateOfBirth());
		playerToBeUpdated.setFirstName(player.getFirstName());
		playerToBeUpdated.setLastName(player.getLastName());
		playerToBeUpdated.setSpeed(player.getSpeed());
		playerToBeUpdated.setTeamId(player.getTeamId());
	}
}


/*
public void add(Player player) {
if(players.stream()
		.filter(p -> p.getId() == player.getId())
		.findFirst()
		.isPresent()) {
	throw new RuntimeException("Pilkarz o podanym id ju� istnieje");
}
players.add(player);
savePlayersToFile();
}

public void update(Player player) {
Optional<Player> optionalPlayer = players.stream()
		.filter(t -> t.getId() == player.getId())
		.findFirst();
if(!optionalPlayer.isPresent()) {
	throw new RuntimeException("Pi�karz o podanym id nie istnieje");
}
Player playerToBeUpdated = optionalPlayer.get();
playerToBeUpdated.setDateOfBirth(player.getDateOfBirth());
playerToBeUpdated.setFirstName(player.getFirstName());
playerToBeUpdated.setLastName(player.getLastName());
playerToBeUpdated.setSpeed(player.getSpeed());
playerToBeUpdated.setTeamId(player.getTeamId());
savePlayersToFile();
}

public void delete(int id) {
Optional<Player> optionalPlayer = players.stream()
		.filter(p -> p.getId() == id)
		.findFirst();
if(!optionalPlayer.isPresent()) {
	throw new RuntimeException("Pilkarz o podanym id nie istnieje");
}
players.remove(optionalPlayer.get());
savePlayersToFile();
}

public List<Player> getByNamePart(String namePart) {
return players.stream()
		.filter(p -> (
					p.getFirstName() + " " + 
					p.getLastName() + " " + 
					p.getFirstName()).indexOf(namePart) > -1)
		.map(p -> new Player(p))
		.collect(Collectors.toList());
}
*/