package football.repositories.files;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import football.Utils;
import football.entities.Player;

public class PlayerRepository extends AbstractRepository<Player> {
	private final static String FILE_NAME = "players.csv";
	
	public PlayerRepository() {
		super();
	}

	public void updateFieldsValues(Player playerToBeUpdated, Player player) {
		playerToBeUpdated.setDateOfBirth(player.getDateOfBirth());
		playerToBeUpdated.setFirstName(player.getFirstName());
		playerToBeUpdated.setLastName(player.getLastName());
		playerToBeUpdated.setSpeed(player.getSpeed());
		playerToBeUpdated.setTeamId(player.getTeamId());
	}

	public List<Player> getByNamePart(String namePart) {
		return entities.stream()
				.filter(p -> (p.getFirstName() + " " + p.getLastName() + " " + p.getFirstName()).indexOf(namePart) > -1)
				.map(p -> new Player(p)).collect(Collectors.toList());
	}

	@Override
	protected String getFileLineFromEntity(Player player) {
		List<String> words = new ArrayList<>();
		words.add(Utils.getTextFromInteger(player.getId(), "Nieprawid這we id pi趾arza"));
		words.add(player.getFirstName());
		words.add(player.getLastName());
		words.add(Utils.getTextFromDate(player.getDateOfBirth(), "Nieprawid這wa data urodzenia zawodnika"));
		words.add(Integer.toString(player.getSpeed()));
		words.add(Utils.getTextFromNullableInteger(player.getTeamId()));
		return String.join(",", words);
	}

	@Override
	protected Player createEntity(String fileLine) {
		List<String> words = Utils.splitToTrimmedList(fileLine, ",", 6,
				"Nieprawid這wa liczba argument闚 w linii opisuj鉍ej gracza");
		Integer id = Utils.getIntegerFromString(words.get(0), "Nieprawid這wy format liczby w linii opisuj鉍ej gracza");
		String firstName = words.get(1);
		String lastName = words.get(2);
		Date dateOfBirth = Utils.getDateFromText(words.get(3), "Nieprawid這wy format daty w linii opisuj鉍ej gracza");
		int speed = Utils.getIntegerFromString(words.get(4), "Nieprawid這wy format liczby w linii opisuj鉍ej gracza");

		Integer teamId = Utils.getIntegerFromString(words.get(5),
				"Nieprawid這wy format liczby w linii opisuj鉍ej gracza", true);
		return new Player(id, firstName, lastName, dateOfBirth, speed, teamId);
	}

	@Override
	protected String getFileName() {
		return FILE_NAME;
	}

	@Override
	protected Player copy(Player player) {
		return new Player(player);
	}
}
