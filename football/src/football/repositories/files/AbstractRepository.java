package football.repositories.files;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import football.entities.AbstractEntity;
import football.repositories.Repository;

public abstract class AbstractRepository<T extends AbstractEntity> implements Repository<T> {
	protected List<T> entities;

	protected abstract T createEntity(String fileLine);

	protected abstract String getFileName();
	
	protected abstract String getFileLineFromEntity(T entity);
	
	protected abstract T copy(T entity);

	protected abstract void updateFieldsValues(T entityToBeUpdated, T entity);
	
	public AbstractRepository() {
		try {
			Stream<String> fileLineStream = Files.lines(Paths.get(getFileName()));
			entities = fileLineStream.map(l -> createEntity(l)).collect(Collectors.toList());
			fileLineStream.close();
		} catch (IOException e) {
			throw new RuntimeException("Nieudane sczytanie encji");
		}
	}

	public T get(int id) {
		Optional<T> optionalEntity = entities.stream()
				.filter(t -> t.getId() == id)
				.findFirst();
		if (optionalEntity.isPresent()) {
			return copy(optionalEntity.get());
		} else {
			return null;
		}
	}
	
	public void add(T entity) {
		int id;
		if(entities.size() == 0) {
			id = 0;
		} else {
			id = entities.stream()
					.map(e -> e.getId())
					.max((id1, id2) -> Integer.compare(id1,  id2))
					.get();
		}
		entity.setId(id);
		entities.add(entity);
		saveEntitiesToFile();
	}
	
	public void update(T entity) {
		Optional<T> optionalEntity = entities.stream()
				.filter(t -> t.getId() == entity.getId())
				.findFirst();
		if(!optionalEntity.isPresent()) {
			throw new RuntimeException("Encja o podanym id nie istnieje");
		}
		updateFieldsValues(optionalEntity.get(), entity);
		saveEntitiesToFile();
	}
	
	public void delete(int id) {
		Optional<T> optionalEntity = entities.stream()
				.filter(p -> p.getId() == id)
				.findFirst();
		if(!optionalEntity.isPresent()) {
			throw new RuntimeException("Encja o podanym id nie istnieje");
		}
		entities.remove(optionalEntity.get());
		saveEntitiesToFile();
	}

	private void saveEntitiesToFile() {
		List<String> lines = entities.stream()
				.map(p -> getFileLineFromEntity(p))
				.collect(Collectors.toList());
		try {
			Files.write(Paths.get(getFileName()), 
					lines, 
					Charset.forName("UTF-8"), 
					StandardOpenOption.TRUNCATE_EXISTING);
		} catch (IOException e) {
			throw new RuntimeException("B��d zapisu do pliku");
		}
	}
}
