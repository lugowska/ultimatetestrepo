package football.repositories.files;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import football.Utils;
import football.entities.Team;

public class TeamRepository extends AbstractRepository<Team> {
	private final static String FILE_NAME = "teams.csv";

	public TeamRepository() {
		super();
	}

	@Override
	protected String getFileLineFromEntity(Team team) {
		List<String> words = new ArrayList<>();
		words.add(Utils.getTextFromInteger(team.getId(), "Nieprawidłowe id drużyny"));
		words.add(team.getName());
		words.add(Utils.getTextFromDate(team.getFoundationDate(), "Nieprawidłowa data założenia drużyny"));
		return String.join(",", words);
	}
	
	@Override
	protected void updateFieldsValues(Team entityToBeUpdated, Team entity) {
		entityToBeUpdated.setFoundationDate(entity.getFoundationDate());
		entityToBeUpdated.setName(entity.getName());
	}

	@Override
	protected Team createEntity(String fileLine) {
		List<String> words = Utils.splitToTrimmedList(fileLine, ",", 3,
				"Nieprawidłowa liczba argumentów w linii opisującej drużynę");
		Integer id = Utils.getIntegerFromString(words.get(0),
				"Nieprawidłowy format liczby w linii opisującej id druzyny");
		String name = words.get(1);
		Date foundationDate = Utils.getDateFromText(words.get(2),
				"Nieprawidłowy format daty w linii opisującej drużynę");
		return new Team(id, name, foundationDate);
	}

	@Override
	protected String getFileName() {
		return FILE_NAME;
	}

	@Override
	protected Team copy(Team entity) {
		return new Team(entity);
	}
}
