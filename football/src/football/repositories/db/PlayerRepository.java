/*package football.repositories.db;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.annotation.Generated;

import football.AbstractRepository;
import football.DBConnection;
import football.MyUtils;
import football.Player;

public class PlayerRepository extends AbstractRepository<Team>{
	private CallableStatement addCallableStatement;
	private CallableStatement deleteCallableStatement;
	private PreparedStatement getPreparedStatement;
	
	public PlayerRepository() {
		Connection connection = DBConnection.getConnection();
		try {
			addCallableStatement = connection.prepareCall("{CALL nowy_pilkarz(?,?,?,?,?,?)}");
			deleteCallableStatement = connection.prepareCall("{CALL usun_pilkarza(?)}");
			getPreparedStatement = connection.prepareStatement("" +
					"SELECT * " +
					"FROM pilkarz " +
					"WHERE id_pilkarz = ?");
		} catch (SQLException e) {
			throw new RuntimeException("B��d inicjalizacji " + PlayerRepository.class.getName());
		}
	}
	
	public void add(Player player) {
		java.util.Date date = player.getDateOfBirth();
		try {
			addCallableStatement.setString(1, player.getFirstName());
			addCallableStatement.setString(2, player.getLastName());
			addCallableStatement.setDate(3, new java.sql.Date(date.getTime())); 
			addCallableStatement.setInt(4, 10);
			addCallableStatement.setInt(5, player.getSpeed());
			addCallableStatement.setInt(6, player.getTeamId());
			addCallableStatement.executeQuery();
			addCallableStatement.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public void delete(int id) {
		try {
			deleteCallableStatement.setInt(1, id);
			deleteCallableStatement.executeQuery();
			deleteCallableStatement.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public Player get(int id) throws SQLException {
		getPreparedStatement.setInt(1, id);
		ResultSet resultSet = getPreparedStatement.executeQuery();
		if(resultSet.next()) {
			Player player = new Player(				
				resultSet.getInt("id_pilkarz"), 
				resultSet.getString("imie"), 
				resultSet.getString("nazwisko"), 
				resultSet.getDate("data_urodzenia"), 
				resultSet.getInt("szybkosc"), 
				resultSet.getInt("id_aktualnej_druzyny"));
			getPreparedStatement.close();
			return player;
		} else {
			return null;
		}
		
	}
}*/

package football.repositories.db;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import football.entities.Player;

public class PlayerRepository extends AbstractRepository<Player> {

	public PlayerRepository() {
		super(PlayerRepository.class);
	}

	@Override
	protected String getAddCallSql() {
		return "{CALL dodaj_pilkarza(?,?,?,?,?,?,?)}";
	}

	@Override
	protected int getAddCallOutParamIndex() {
		return 7;
	}

	@Override
	protected String getDeleteCallSql() {
		return "{CALL usun_pilkarza(?)}";
	}

	@Override
	protected String getGetPrepStmtSql() {
		return "SELECT * " + 
				"FROM pilkarz " +
				"WHERE id_pilkarz = ?";
	}

	@Override
	protected void setAddCallableStatementParams(Player player) throws SQLException {
		addCallableStatement.setString(1, player.getFirstName());
		addCallableStatement.setString(2, player.getLastName());
		addCallableStatement.setDate(3, 
				new java.sql.Date(player.getDateOfBirth().getTime()));
		addCallableStatement.setInt(4, player.getSpeed());
		addCallableStatement.setInt(5, player.getSpeed());
		addCallableStatement.setInt(6, player.getTeamId());
	}

	@Override
	protected Player getEntityFromResultSet(ResultSet resultSet) throws SQLException {
		return new Player(
				resultSet.getInt("id_pilkarz"),
				resultSet.getString("imie"),
				resultSet.getString("nazwisko"),
				new Date(resultSet.getDate("data_urodzenia").getTime()),
				resultSet.getInt("szybkosc"),
				resultSet.getInt("id_aktualnej_druzyny")
			);
	}
}

