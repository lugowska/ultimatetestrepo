package football.repositories.db;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;

import football.entities.Team;

public class TeamRepository extends AbstractRepository<Team> {
	public TeamRepository() {
		super(TeamRepository.class);
	}

	@Override
	protected String getAddCallSql() {
		return "{CALL dodaj_druzyne(?,?,?)}";
	}

	@Override
	protected int getAddCallOutParamIndex() {
		return 3;
	}

	@Override
	protected String getDeleteCallSql() {
		return "{CALL usun_druzyne(?)}";
	}

	@Override
	protected String getGetPrepStmtSql() {
		return "SELECT * " + 
				"FROM druzyna " +
				"WHERE id_druzyna = ?";
	}

	@Override
	protected void setAddCallableStatementParams(Team entity) throws SQLException {
		addCallableStatement.setString(1, entity.getName());
		addCallableStatement.setDate(2, new Date(entity.getFoundationDate().getTime()));
	}

	@Override
	protected Team getEntityFromResultSet(ResultSet resultSet) throws SQLException {
		return new Team(
				resultSet.getInt("id_druzyna"),
				resultSet.getString("nazwa"), 
				new java.util.Date(resultSet.getDate("data_zalozenia").getTime()));
	}
}
