package football.repositories;

public interface Repository<T> {
	public void add(T entity);
	public void delete(int id);
	public T get(int id);
}
