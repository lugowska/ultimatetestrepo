package football;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Team extends AbstractEntity {
	private String name;
	private Date foundationDate;
	
	@Override
	public String toString() {
		DateFormat df = new SimpleDateFormat("yyyy");
		return "Dru�yna nr " + getId() + ": " + name + ", za�." + df.format(foundationDate);
	}

	public Team(Integer id, String name, Date foundationDate) {
		super(id);
		this.name = name;
		this.foundationDate = foundationDate;
	}
	
	public Team(Team team) {
		super(checkIfNotNull(team));
		this.name = team.getName();
		this.foundationDate = team.getFoundationDate();
	}

	public String getName() {
		return name;
	}

	public Date getFoundationDate() {
		return foundationDate;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setFoundationDate(Date foundationDate) {
		this.foundationDate = foundationDate;
	}

	
}
