package football;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class TeamRepository extends AbstractRepository<Team> {
	private static final String FILE_NAME = "teams.csv";

	@Override
	protected String getFileName() {
		return FILE_NAME;
	}
	public TeamRepository() {
		super();
	}

	@Override	
	protected String getFileLineFromEntity(Team team) {
		DateFormat df = new SimpleDateFormat("yyyy");
		List<String> words = new ArrayList<>();
		words.add(team.getId().toString());
		words.add(team.getName());
		words.add(df.format(team.getFoundationDate()));
		String fileLine = String.join(",", words);
		return fileLine;
	}
	@Override		
	protected Team createEntity(String fileLine) {
		List<String> words = MyUtils.splitToTrimmedList(fileLine, ",");
		Integer id = MyUtils.getIntegerFromString(words.get(0), "Ostrzeżenie 1");
		String name = words.get(1);
		Date foundationDate = MyUtils.getYearFromText(words.get(2), "Ostrzeżenie 2");

		return new Team(id, name, foundationDate);
	}

	@Override
	protected Team copy(Team team) {
		return new Team(team);
	}
	
	@Override
	public void saveEntitiesToFile() {
		
	}
	@Override
	public Team get(int id) {
		// TODO Auto-generated method stub
		return super.get(id);
	}
	@Override
	public void delete(int id) {
		// TODO Auto-generated method stub
		super.delete(id);
	}
	@Override
	public void add(Team item) {
		// TODO Auto-generated method stub
		super.add(item);
	}
	
	@Override
	protected void updateFieldsValues(Team entityToBeUpdated, Team entity) {
		entityToBeUpdated.setFoundationDate(entity.getFoundationDate());
		entityToBeUpdated.setName(entity.getName());
	}
}
