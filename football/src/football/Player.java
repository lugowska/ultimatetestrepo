package football;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Player extends AbstractEntity {

	private String firstName;
	private String lastName;
	private Date dateOfBirth;
	private int speed;
	private Integer teamId;

	@Override
	public String toString() {
		DateFormat df = new SimpleDateFormat("dd.MM.yyyy");
		return "Gracz nr "+ getId() + ": " + firstName + " " + lastName + ", ur. " + df.format(dateOfBirth) + ", szybko�� - "
				+ speed + ", nr dru�yny - " + teamId;
	}

	public Player(Integer id, String firstName, String lastName, Date dateOfBirth, int speed, Integer teamId) {
		super(id);
		this.firstName = firstName;
		this.lastName = lastName;
		this.dateOfBirth = dateOfBirth;
		this.speed = speed;
		this.teamId = teamId;
	}

	public Player(Player player) {
		super(checkIfNotNull(player));
		this.firstName = player.getFirstName();
		this.lastName = player.getLastName();
		this.dateOfBirth = player.dateOfBirth;
		this.speed = player.getSpeed();
		this.teamId = player.getTeamId();
	}
	
	
	public String getFirstName() {
		return firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public Date getDateOfBirth() {
		return dateOfBirth;
	}

	public int getSpeed() {
		return speed;
	}

	public Integer getTeamId() {
		return teamId;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	public void setSpeed(int speed) {
		this.speed = speed;
	}

	public void setTeamId(Integer teamId) {
		this.teamId = teamId;
	}


	
	
}
