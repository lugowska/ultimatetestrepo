package football.entities;

import java.util.Date;

public class Player extends AbstractEntity {
	private String firstName;
	private String lastName;
	private Date dateOfBirth;
	private int speed;
	private Integer teamId;

	public Player(Integer id, String firstName, String lastName, Date dateOfBirth, int speed, Integer teamId) {
		super(id);
		this.firstName = firstName;
		this.lastName = lastName;
		this.dateOfBirth = dateOfBirth;
		this.speed = speed;
		this.teamId = teamId;
	}
	
	public Player(Player player) {
		super(checkIfNotNull(player));
		this.firstName = player.getFirstName();
		this.lastName = player.getLastName();
		this.speed = player.getSpeed();
		this.dateOfBirth = player.getDateOfBirth();
		this.teamId = player.getTeamId();
	}

	public String getFirstName() {
		return firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public Date getDateOfBirth() {
		return dateOfBirth;
	}

	public int getSpeed() {
		return speed;
	}

	public Integer getTeamId() {
		return teamId;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	public void setSpeed(int speed) {
		this.speed = speed;
	}

	public void setTeamId(Integer teamId) {
		this.teamId = teamId;
	}	
	
}
