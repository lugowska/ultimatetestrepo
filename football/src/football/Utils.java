package football;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

public class Utils {
	private static SimpleDateFormat SDF = new SimpleDateFormat("yyyy-MM-dd");
	
	public static List<String> splitToTrimmedList(String line, String separator) {
		return Arrays.asList(line.split(separator))
				.stream()
				.map(w -> w.trim())
				.collect(Collectors.toList());
	}
	
	public static String getTextFromInteger(Integer value, String errorMessageText) {
		if(value == null) {
			throw new RuntimeException(errorMessageText);
		}
		return Integer.toString(value);
	}
	
	public static String getTextFromNullableInteger(Integer value) {
		if(value == null) {
			return "null";
		} 
		return Integer.toString(value);
	}
	
	public static String getTextFromDate(Date date, String errorMessageText) {
		if(date == null) {
			throw new RuntimeException(errorMessageText);
		}
		return SDF.format(date);
	}
	
	public static Date getDateFromText(String text, String errorMessageText) {
		try {
			return SDF.parse(text);
		} catch (ParseException e) {
			throw new RuntimeException(errorMessageText);
		}
	}
	
	public static Integer getIntegerFromString(String text, String errorMessageText) {
		try {
			return Integer.parseInt(text);
		} catch (NumberFormatException e) {
			throw new RuntimeException(errorMessageText);
		}
	}
	
	public static List<String> splitToTrimmedList(String line, String separator, int expectedSize, String errorMessageText) {
		List<String> words = splitToTrimmedList(line, separator);

		if (words.size() != expectedSize) {
			throw new RuntimeException(errorMessageText);
		}
		return words;
	}
	
	public static Integer getIntegerFromString(String text, String errorMessageText, boolean nullable){
		if(nullable && text != null && text.equals("null")) {
			return null;
		}
		return getIntegerFromString(text,errorMessageText);
	}
}
