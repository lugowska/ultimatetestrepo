package football;

public abstract class AbstractEntity {
	private Integer id;

	public AbstractEntity(Integer id) {
		super();
		this.id = id;
	}

	public Integer getId() {
		return id;
	}
	
	protected static Integer checkIfNotNull(AbstractEntity entity) {
		if(entity == null) {
			throw new RuntimeException("Nieprawidłowy element konstruktora!");
		}
		return entity.getId();
	}
	
	
}
