package football;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

public class MyUtils {

	public static List<String> splitToTrimmedList(String line, String separator) {
		List<String> words = Arrays.asList(line.split(separator))
				.stream()
				.map(w -> w.trim())
				.collect(Collectors.toList());
		return words;
	}
	
	public static Date getDateFromText(String text, String errorMessageText) {
		SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy");
		Date date;
		try {
			date = sdf.parse(text);
		} catch (ParseException e) {
			throw new RuntimeException(errorMessageText);
		}
		return date;
	}
	
	public static Date getYearFromText(String text, String errorMessageText) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy");
		Date date;
		try {
			date = sdf.parse(text);
		} catch (ParseException e) {
			throw new RuntimeException(errorMessageText);
		}
		return date;
	}
	
	public static Integer getIntegerFromString(String text, String errorMessageText) {
		Integer anInteger;
		try {
			anInteger = Integer.parseInt(text);
		} catch (NumberFormatException e) {
			throw new RuntimeException(errorMessageText);
		}
		return anInteger;
	}

	public static Integer getIntFromString(String text, String errorMessageText) {
		int anInt;
		try {
			anInt = Integer.parseInt(text);
		} catch (NumberFormatException e) {
			throw new RuntimeException(errorMessageText);
		}
		return anInt;
	}
}

