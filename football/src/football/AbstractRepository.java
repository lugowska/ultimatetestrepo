package football;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public abstract class AbstractRepository<T extends AbstractEntity> {
	protected List<T> items;
	
	protected abstract T createEntity(String fileLine);
	protected abstract String getFileName();
	protected abstract T copy(T entity);
	protected abstract String getFileLineFromEntity(T entity);
	protected abstract void updateFieldsValues(T entityToBeUpdated, T entity);
	

	public AbstractRepository() {
		try {
			Stream<String> fileLineStream = Files.lines(Paths.get(getFileName()));
			items = fileLineStream.map(l -> createEntity(l)).collect(Collectors.toList());
			fileLineStream.close();
			System.out.println(items);
		} catch (IOException e) {
			throw new RuntimeException("Nieudany odczyt pliku!");
		}
	}
	
	public T get(int id) {
		Optional<T> optionalEntity = items.stream()
				.filter(t -> t.getId().equals(id))
				.findFirst();
		if (optionalEntity.isPresent()) {			
			return copy(optionalEntity.get());
		} else {
			return null;
		}
	}
	
	public void saveEntitiesToFile() {
		List<String> lines = new ArrayList<>();
		lines = items.stream().map(i -> getFileLineFromEntity(i)).collect(Collectors.toList());
		try {
			Files.write(Paths.get(getFileName()),
					lines,
					Charset.forName("UTF-8"),
					StandardOpenOption.TRUNCATE_EXISTING);
		} catch (IOException e) {
			throw new RuntimeException("B��d zapisu!");
		}
	}
		
	public void delete(int id) {
		Optional<T> optionalEntity = items.stream()
				.filter(t -> t.getId() == id)
				.findFirst();
		if(!optionalEntity.isPresent()) {
			throw new RuntimeException("Element o podanym id nie istnieje");
		}
		items.remove(optionalEntity.get());
		saveEntitiesToFile();
		}
	
	public void add(T entity) {
		Optional<T> optionalEntity = items.stream()
				.filter(i -> i.getId().equals(entity.getId()))
				.findFirst();
			if(optionalEntity.isPresent()) {
				throw new RuntimeException("Id u�yte.");
			} else {				
				items.add(entity);
				saveEntitiesToFile();
			}
	}
	
	public void update(T entity) {
		Optional<T> optionalEntity = items.stream()
				.filter(t -> t.getId() == entity.getId())
				.findFirst();
		if(!optionalEntity.isPresent()) {
			throw new RuntimeException("Element o podanym id nie istnieje");
		}
		updateFieldsValues(optionalEntity.get(), entity);
		saveEntitiesToFile();
		}
	/*
	 * public void update(T entity) {
		Optional<T> optionalEntity = entities.stream()
				.filter(t -> t.getId() == entity.getId())
				.findFirst();
		if(!optionalEntity.isPresent()) {
			throw new RuntimeException("Encja o podanym id nie istnieje");
		}
		updateFieldsValues(optionalEntity.get(), entity);
		saveEntitiesToFile();
	}
	
	 */
}
